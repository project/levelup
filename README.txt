This module allows awarding points to users, and setting "level up" limits where
users are awarded new roles.

The module is a light-weight alternative to the User Points module/framework,
leveraging Rules to allow creating events and conditions for awarding points.
See this issue for a discussion about this module in comparison with User
Points: http://drupal.org/1490990.

How this module works, briefly:

* All users have a field 'level up points', containing their points. (Only users
  with the 'configure level up' permission may edit this field manually.)
* There is an entity type 'level up limit', with properties for storing point
  limit and role to award.
* Whenever a user account is saved, Rules checks against the level up limits and
  adds/removes related roles.
* Reaction rules (or custom modules) are recommended to call the Rules component
  'award Level up points', provided by this module – but is not required for
  level up/down to work.

This module mainly consists of a few features, and small pieces of custom code:

* An entity type 'level up limit' is provided by the Entity Construction Kit
  module. The entity has two properties – points and role – which both are
  integers. Small custom functions expose these properties on the entity edit
  form.
* The reaction rule 'level up actions on account update' fires whenever user
  accounts are updated. It loads all level limit entities, loops through them
  and calls the rule set 'check for level up' for each level. The rule set, in
  turn, takes care of level up/down.
* There is a field 'levelup_points' added to user accounts. A form alter in this
  module blocks this field for non-authorized users. The module also provides a
  new permission ('Configure Level up'), for managing this.
* The module also provides an action for loading *all* level up limit entities
  into Rules.

What this module does not do out of the box, compared to User Points:

* Store a record for each adding/subtracting of points.
* Allow moderated points.
* Categorization of points.
* Expiration of points.

(Also, this module does not check for level up for existing users when level up
point limits are changed. That is, if you lower the limit for level one from 100
to 50 points, a user with 75 points will not level up.)

Benefits from this approach for awarding points:

* You can use all events available to Rules to trigger point awarding. (And if
  you need to declare new events, these will be available and useful in more
  contexts than just for awarding points.)
* Rules allows for more complex conditions. (And again, if you need to provide
  new conditions these will be useful for a larger audience.)
* If you want follow-up actions when points are awarded or when users level
  up/down, these could be added by editing the relevant Rules components.
* If you want to add new information related to levels, this can be done by
  adding new fields to the Level up limit entities. It is also possible to add
  new bundles, to have differentiated levels.

Some examples on how Rules can be used to award points can be found here:
* http://tinyurl.com/rulesmastery
* http://www.youtube.com/playlist?list=PL84B6FFA4F7ACAD57&feature=view_all

Potential improvements for future versions:

* Creating an entity type for storing point transactions, thereby keeping
  information like time, number of points, awarded user and possibly also the
  caller function. This would also allow for moderated points and point
  expiration. Downside is that on any large site it would also create a huge
  mass of entities to store, thereby no longer making this module a light-weight
  alternative to User Points.
