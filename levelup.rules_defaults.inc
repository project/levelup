<?php
/**
 * @file
 * levelup.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function levelup_default_rules_configuration() {
  $items = array();
  $items['rules_levelup_award_points'] = entity_import('rules_config', '{ "rules_levelup_award_points" : {
      "LABEL" : "Award level up points",
      "PLUGIN" : "rule set",
      "TAGS" : [ "Level up" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "account" : { "label" : "User to award", "type" : "user" },
        "points" : { "label" : "Number of points", "type" : "integer" }
      },
      "RULES" : [
        { "RULE" : {
            "IF" : [ { "data_is_empty" : { "data" : [ "account:levelup-points" ] } } ],
            "DO" : [
              { "data_set" : { "data" : [ "account:levelup-points" ], "value" : "0" } }
            ],
            "LABEL" : "Verify point field"
          }
        },
        { "RULE" : {
            "PROVIDE" : { "result" : { "points_new" : "New points" } },
            "DO" : [
              { "data_calc" : {
                  "USING" : {
                    "input_1" : [ "account:levelup-points" ],
                    "op" : "+",
                    "input_2" : [ "points" ]
                  },
                  "PROVIDE" : { "result" : { "points_new" : "New points" } }
                }
              },
              { "data_set" : { "data" : [ "account:levelup-points" ], "value" : [ "points-new" ] } }
            ],
            "LABEL" : "Add points"
          }
        }
      ]
    }
  }');
  $items['rules_levelup_check_for_level_up'] = entity_import('rules_config', '{ "rules_levelup_check_for_level_up" : {
      "LABEL" : "Check for level up",
      "PLUGIN" : "rule set",
      "TAGS" : [ "Level up" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "levelup_limit" : { "label" : "Level up limit", "type" : "levelup_level_limit" },
        "account_updated" : { "label" : "Updated account", "type" : "user" },
        "account_unsaved" : { "label" : "Unsaved account", "type" : "user" }
      },
      "RULES" : [
        { "RULE" : {
            "IF" : [
              { "data_is" : {
                  "data" : [ "account-unsaved:levelup-points" ],
                  "op" : "\\u003C",
                  "value" : [ "levelup-limit:points" ]
                }
              },
              { "NOT data_is" : {
                  "data" : [ "account-updated:levelup-points" ],
                  "op" : "\\u003C",
                  "value" : [ "levelup-limit:points" ]
                }
              }
            ],
            "DO" : [
              { "list_add" : {
                  "list" : [ "account-updated:roles" ],
                  "item" : [ "levelup-limit:role" ],
                  "unique" : 1
                }
              },
              { "entity_save" : { "data" : [ "account-updated" ] } }
            ],
            "LABEL" : "Check for level up"
          }
        },
        { "RULE" : {
            "IF" : [
              { "data_is" : {
                  "data" : [ "account-updated:levelup-points" ],
                  "op" : "\\u003C",
                  "value" : [ "levelup-limit:points" ]
                }
              },
              { "NOT data_is" : {
                  "data" : [ "account-unsaved:levelup-points" ],
                  "op" : "\\u003C",
                  "value" : [ "levelup-limit:points" ]
                }
              }
            ],
            "DO" : [
              { "list_remove" : {
                  "list" : [ "account-updated:roles" ],
                  "item" : [ "levelup-limit:role" ]
                }
              },
              { "entity_save" : { "data" : [ "account-updated" ] } }
            ],
            "LABEL" : "Check for level down"
          }
        }
      ]
    }
  }');
  $items['rules_levelup_user_update'] = entity_import('rules_config', '{ "rules_levelup_user_update" : {
      "LABEL" : "Level up actions on account update",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "Level up" ],
      "REQUIRES" : [ "levelup", "rules" ],
      "ON" : [ "user_presave" ],
      "DO" : [
        { "levelup_get_all_levels" : { "PROVIDE" : { "levels" : { "levels" : "Level up levels" } } } },
        { "LOOP" : {
            "USING" : { "list" : [ "levels" ] },
            "ITEM" : { "level" : "Level" },
            "DO" : [
              { "component_rules_levelup_check_for_level_up" : {
                  "levelup_limit" : [ "level" ],
                  "account_updated" : [ "account" ],
                  "account_unsaved" : [ "account-unchanged" ]
                }
              }
            ]
          }
        }
      ]
    }
  }');
  return $items;
}
