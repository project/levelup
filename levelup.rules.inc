<?php

/**
 * @file
 * Contains an action for loading entities describing the Level up limits.
 */

/**
 * Implements hook_rules_action_info(). 
 */
function levelup_rules_action_info() {
  $actions = array(
    'levelup_get_all_levels' => array(
      'label' => t('Load all Level up levels'),
      'group' => t('Level up'),
      'provides' => array(
        'levels' => array(
          'type' => 'list<levelup_level_limit>',
          'label' => t('Level up levels'),
        ),
      ),
    )
  );

  return $actions;
}

/**
 * Fetches all Level up level entities, to be used by Rules. 
 */
function levelup_get_all_levels() {
  $levels = entity_load('levelup_level_limit', FALSE, array('type' => 'levelup_level_limit'));
  return array(
    'levels' => $levels,
  );
}
