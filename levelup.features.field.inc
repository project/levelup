<?php
/**
 * @file
 * levelup.features.field.inc
 */

/**
 * Implements hook_field_default_fields().
 */
function levelup_field_default_fields() {
  $fields = array();

  // Exported field: 'user-user-levelup_points'
  $fields['user-user-levelup_points'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'levelup_points',
      'foreign keys' => array(),
      'indexes' => array(),
      'module' => 'number',
      'settings' => array(),
      'translatable' => '0',
      'type' => 'number_integer',
    ),
    'field_instance' => array(
      'bundle' => 'user',
      'default_value' => array(
        0 => array(
          'value' => '0',
        ),
      ),
      'deleted' => '0',
      'description' => 'The Level up points for this user. This field can only be edited by users with the \'configure Level up\' permission.',
      'display' => array(
        'default' => array(
          'label' => 'inline',
          'module' => 'number',
          'settings' => array(
            'decimal_separator' => '.',
            'prefix_suffix' => TRUE,
            'scale' => 0,
            'thousand_separator' => ' ',
          ),
          'type' => 'number_integer',
          'weight' => '0',
        ),
      ),
      'entity_type' => 'user',
      'field_name' => 'levelup_points',
      'label' => 'Points',
      'required' => 0,
      'settings' => array(
        'max' => '',
        'min' => '',
        'prefix' => '',
        'suffix' => '',
        'user_register_form' => 0,
      ),
      'widget' => array(
        'active' => 0,
        'module' => 'number',
        'settings' => array(),
        'type' => 'number',
        'weight' => '1',
      ),
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Points');
  t('The Level up points for this user. This field can only be edited by users with the \'configure Level up\' permission.');

  return $fields;
}
