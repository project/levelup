<?php
/**
 * @file
 * levelup.features.inc
 */

/**
 * Implements hook_eck_bundle_info().
 */
function levelup_eck_bundle_info() {
  $items = array(
  'levelup_level_limit_levelup_level_limit' => array(
  'machine_name' => 'levelup_level_limit_levelup_level_limit',
  'entity_type' => 'levelup_level_limit',
  'name' => 'levelup_level_limit',
  'label' => 'Level up limit',
),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function levelup_eck_entity_type_info() {
$items = array(
       'levelup_level_limit' => array(
  'name' => 'levelup_level_limit',
  'label' => 'Level up limit',
  'properties' => array(
    'title' => array(
      'label' => 'Title',
      'type' => 'text',
      'behavior' => 'title',
    ),
    'points' => array(
      'label' => 'Point limit',
      'type' => 'integer',
      'behavior' => '',
    ),
    'role' => array(
      'label' => 'Role',
      'type' => 'integer',
      'behavior' => '',
    ),
  ),
),
  );
  return $items;
}
